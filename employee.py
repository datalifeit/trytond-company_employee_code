# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, DeactivableMixin
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Id
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Employee(DeactivableMixin, metaclass=PoolMeta):
    __name__ = 'company.employee'

    code = fields.Char('Code', required=True, select=True,
        states={
            'readonly': Eval('code_readonly', True),
            },
        depends=['code_readonly'])
    code_readonly = fields.Function(fields.Boolean('Code Readonly'),
        'get_code_readonly')

    @staticmethod
    def default_code_readonly():
        Company = Pool().get('company.company')
        if not Transaction().context.get('company', None):
            return True
        return bool(Company(Transaction().context['company']
            ).employee_sequence)

    def get_code_readonly(self, name):
        return True

    def get_rec_name(self, name):
        if not self.party.name:
            return '[' + self.code + ']'
        return self.party.name

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('code',) + tuple(clause[1:]),
            ('party.name',) + tuple(clause[1:])]

    @classmethod
    def create(cls, vlist):
        Company = Pool().get('company.company')

        vlist = [x.copy() for x in vlist]
        companies = Company.search([])
        companies = {c.id: c.employee_sequence for c in companies}
        for values in vlist:
            if not values.get('code'):
                if not companies[values['company']]:
                    raise UserError(gettext(
                        'company_employee_code.'
                        'msg_company_employee_employee_sequence',
                        company=Company(values['company']).rec_name))
                seq = companies[values['company']]
                values['code'] = seq.get()
        return super(Employee, cls).create(vlist)


class Company(metaclass=PoolMeta):
    __name__ = 'company.company'

    employee_sequence = fields.Many2One('ir.sequence',
        'Employee Sequence', domain=[
            ('sequence_type', '=', Id('company_employee_code',
                'sequence_type_employee'))
        ])
