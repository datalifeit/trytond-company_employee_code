==============================
Company Employee Code Scenario
==============================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard


Install company_employee_code::

    >>> config = activate_modules('company_employee_code')
